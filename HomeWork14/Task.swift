//
//  Task.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 02.10.2021.
//

import UIKit
import RealmSwift

class Task: Object {
    @objc dynamic var id = Int()
    @objc dynamic var name: String = String()
    @objc dynamic var descript: String = String()
    
    static func getTask(id: Int, name: String, descript: String) -> Task{
        let task = Task()
        task.id = id
        task.name = name
        task.descript = descript
        
        return task
    }
}

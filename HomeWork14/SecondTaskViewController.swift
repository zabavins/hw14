//
//  SecondTaskViewController.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 23.09.2021.
//

import UIKit
import RealmSwift

class SecondTaskViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var deskTextField: UITextField!
    @IBOutlet weak var addTaskButton: UIButton!
    @IBOutlet weak var todoTableView: UITableView!
    
    let realmInstance = try! Realm()
    
    var tasks:[Task] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAddButton()
        tasks = getTasks()
    }
    
    @IBAction func addTask(_ sender: Any) {
        if let name = nameTextField.text, let desk = deskTextField.text, !name.isEmpty {
            let lastId = tasks.first?.id ?? 0
            let task = Task.getTask(id: lastId + 1, name: name, descript: desk)
            tasks.insert(task, at: 0)
            todoTableView.beginUpdates()
            todoTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .right)
            todoTableView.endUpdates()

            try! realmInstance.write{
                realmInstance.add(task)
            }
            
            nameTextField.text = nil
            deskTextField.text = nil
        }
    }
    
    @IBAction func deleteTask(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: todoTableView)
        
        guard let indexPath = todoTableView.indexPathForRow(at: point) else { return }
        
        let taskId = tasks[indexPath.row].id
        let task = realmInstance.objects(Task.self).filter("id == \(taskId)")
        tasks.remove(at: indexPath.row)
        
        try! realmInstance.write{
            realmInstance.delete(task)
        }
        todoTableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .left)
    }
    
    func getTasks() -> [Task] {
        var tasks = [Task]()
        for task in realmInstance.objects(Task.self).sorted(byKeyPath: "id", ascending: false){
            tasks.append(task)
        }
        return tasks
    }
    
    func setUpAddButton() {
        addTaskButton.layer.cornerRadius = 5
    }

}
extension SecondTaskViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = todoTableView.dequeueReusableCell(withIdentifier: "todoCell", for: indexPath) as? TaskTableViewCell else { return UITableViewCell () }
        
        cell.clearParams()
        
        let task = tasks[indexPath.row]
        
        cell.setTaskParams(name: task.name, desk: task.descript)
        
        return cell
        
    }
    
    
}

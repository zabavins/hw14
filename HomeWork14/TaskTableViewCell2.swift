//
//  TaskTableViewCell2.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 02.10.2021.
//

import UIKit

class TaskTableViewCell2: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deskLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTaskParams(name: String, desk: String) {
        nameLabel.text = name
        deskLabel.text = desk
    }
    func clearParams() {
        nameLabel.text = nil
        deskLabel.text = nil
    }
}

//
//  ViewController.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 23.09.2021.
//

import UIKit

class TaskOneViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var secondnameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    let kNameKey = "userName"
    let kSecondnameKey = "userSecondName"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        fillFields()
    }

    @IBAction func saveName(_ sender: Any) {
        writeToUserDefaults()
    }
    
    func fillFields() {
        if let name = UserDefaults.standard.string(forKey: kNameKey){
            nameTextField.text = name
        }
        if let secondname = UserDefaults.standard.string(forKey: kSecondnameKey){
            secondnameTextField.text = secondname
        }
        
    }
    
    func writeToUserDefaults() {
        if let name = nameTextField.text {
            UserDefaults.standard.setValue(name, forKey: kNameKey)
        }
        if let secondname = secondnameTextField.text {
            UserDefaults.standard.setValue(secondname, forKey: kSecondnameKey)
        }
        
    }
    
    func setupButton() {
        saveButton.layer.cornerRadius = 5
    }

}


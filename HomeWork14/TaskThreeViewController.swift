//
//  TaskThreeViewController.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 02.10.2021.
//

import UIKit
import CoreData

class TaskThreeViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var deskTextField: UITextField!
    @IBOutlet weak var addTaskButton: UIButton!
    @IBOutlet weak var todoTableView: UITableView!
    
    var tasks:[Task2] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAddButton()
        tasks = getTasks()
    }
    
    @IBAction func addTask(_ sender: Any) {
        if let name = nameTextField.text, let desk = deskTextField.text, !name.isEmpty {
            let lastId = tasks.first?.id ?? 0
            let context = getContext()
            let entity = NSEntityDescription.entity(forEntityName: "Task2", in: context)
            let newTask = NSManagedObject(entity: entity!, insertInto: context)
            
            newTask.setValue(lastId + 1, forKey: "id")
            newTask.setValue(name, forKey: "name")
            newTask.setValue(desk, forKey: "desk")
            
            do {
               try context.save()
              } catch {
               print("Failed saving")
            }
            
            guard let task2Task = newTask as? Task2 else { return }
            debugPrint(type(of: task2Task))
            tasks.insert(task2Task, at: 0)
            todoTableView.beginUpdates()
            todoTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .right)
            todoTableView.endUpdates()
            
            nameTextField.text = nil
            deskTextField.text = nil
            
        }
    }
    
    @IBAction func deleteTask(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: todoTableView)
        
        guard let indexPath = todoTableView.indexPathForRow(at: point) else { return }
        
        let context = getContext()
        context.delete(tasks[indexPath.row] as NSManagedObject)
        do {
           try context.save()
          } catch {
           print("Failed saving")
        }
        tasks.remove(at: indexPath.row)
        todoTableView.deleteRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .left)
    }
    
    func getTasks() -> [Task2] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Task2")
        let context = getContext()
        //request.predicate = NSPredicate(format: "id = %@", "1")
        request.returnsObjectsAsFaults = false
        var tasks = [Task2]()
        do {
            let result = try context.fetch(request)
            for task in result {
                if let newTask = task as? Task2 {
                    tasks.append(newTask)
                }
            }
        } catch {
            
            print("Failed")
        }
        return tasks
    }
    
    func setUpAddButton() {
        addTaskButton.layer.cornerRadius = 5
    }
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return context
    }

}
extension TaskThreeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = todoTableView.dequeueReusableCell(withIdentifier: "todoCell2", for: indexPath) as? TaskTableViewCell2 else { return UITableViewCell () }
        
        cell.clearParams()
        
        let task = tasks[indexPath.row]
        
        if let name = task.name, let desk = task.desk {
            cell.setTaskParams(name: name, desk: desk)
        }
        
        return cell
        
    }
    
    
}

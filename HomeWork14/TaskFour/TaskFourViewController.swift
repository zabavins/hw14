//
//  TaskFourViewController.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit

class TaskFourViewController: UIViewController {

        @IBOutlet weak var charactersTableView: UITableView!
        @IBOutlet weak var activitiIndicator: UIActivityIndicatorView!
    
        var chList: [Character] = []

        override func viewDidLoad() {
            super.viewDidLoad()
            let loader = CharacterListLoader()
            self.chList = loader.getCharactersListFromRealm()
            self.charactersTableView.reloadData()
            
            if self.chList.count == 0 {
                print("load from server")
                loader.getCharactersList(indicator: activitiIndicator, page: 1, completion: {characterList in
                    if let list = characterList.characters{
                        self.chList = list
                        self.charactersTableView.reloadData()
                    }
                })
            }
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let cell = sender as? CharacterTableViewCell, let index = charactersTableView.indexPath(for: cell){
                if let vc = segue.destination as? CharacterDetailedViewController, segue.identifier == "characterDetailed"{
                    let currentCell = chList[index.row]
                    vc.id = currentCell.id
                }
            }
        }
}
extension TaskFourViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "characterCell") as! CharacterTableViewCell
        
        cell.clearParams()
        
        let item = chList[indexPath.row]
            
        cell.setItemParams(image: item.image, name: item.name, alive: item.status, species: item.species, location: item.location)
            
        //print("Update cell")
        //sleep(1)
        
        return cell
    }
    
    
}

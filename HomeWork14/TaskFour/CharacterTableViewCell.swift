//
//  CharacterTableViewCell.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var aliveIndicatorView: UIView!
    @IBOutlet weak var aliveLabel: UILabel!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func clearParams() {
        self.aliveIndicatorView.backgroundColor = .red
    }
    
    func setItemParams (image: String, name: String, alive: String, species: String, location: String?) {
        
        aliveIndicatorView.layer.cornerRadius = aliveIndicatorView.frame.width / 2
        
        if alive == "Alive" {
            aliveIndicatorView.backgroundColor = .green
        }
        
        //Get image
        let url = URL(string: image)

        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url!){
                DispatchQueue.main.async {
                    self.characterImage.image = UIImage(data: data)
                }
                
            }
        }
        
        nameLabel.text = name
        aliveLabel.text = "\(alive) - \(species)"
        locationTitleLabel.text = "Last known location"
        locationLabel.text = location
    }

}

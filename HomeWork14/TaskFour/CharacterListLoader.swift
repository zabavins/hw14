//
//  CharacterListLoader.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit
import Alamofire
import RealmSwift

class CharacterListLoader: NSObject {
    
    func getCharactersListFromRealm () -> [Character] {
        let realmInstance = try! Realm()
        var characters = [Character]()
        for oneCharacter in realmInstance.objects(Character.self){
            characters.append(oneCharacter)
        }
        return characters
    }
    
    func getCharactersList(indicator: UIActivityIndicatorView, page:Int, completion: @escaping (CharacterList) -> Void) {
        let url:String
        
        switch page {
        case 1:
            url = "https://rickandmortyapi.com/api/character"
        default:
            url = "https://rickandmortyapi.com/api/character?page=" + String(page)
        }
        indicator.isHidden = false
        indicator.startAnimating()
        AF.request(url).validate().responseData {response in
            switch response.result {
            case .success(let value):
                    if let chList = try? JSONDecoder().decode(CharacterList.self, from: value) {
                        DispatchQueue.main.async {
                            let realmInstance = try! Realm()
                            if let characters = chList.characters {
                                for oneCharacter in characters {
                                    let currentCharacter = realmInstance.objects(Character.self).filter("id == \(oneCharacter.id)")
                                    if currentCharacter.count == 0 {
                                        try! realmInstance.write {
                                            realmInstance.add(oneCharacter)
                                        }
                                    }
                                }
                            }
                            indicator.stopAnimating()
                            indicator.isHidden = true
                            completion(chList)
                        }
                    }
            case .failure(let error):
                print(error)
            }
        }
    }
}

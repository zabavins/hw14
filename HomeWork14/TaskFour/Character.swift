//
//  Character.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit
import RealmSwift

class Character:Object, Decodable{
    @objc dynamic var id = Int()
    @objc dynamic var name = String()
    @objc dynamic var status = String()
    @objc dynamic var species = String()
    @objc dynamic var gender = String()
    @objc dynamic var origin = String()
    @objc dynamic var location = String()
    @objc dynamic var image = String()
    @objc dynamic var url = String()
    let episode: List<String>
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case status
        case species
        case gender
        case origin
        case location
        case image
        case episode
        case url
    }
    
    enum OriginLocationCodingKeys: String, CodingKey {
        case name
        case url
    }
    
    override init() {
        self.episode = List<String>()
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try! container.decode(Int.self, forKey: .id)
        self.name = try! container.decode(String.self, forKey: .name)
        self.status = try! container.decode(String.self, forKey: .status)
        self.species = try! container.decode(String.self, forKey: .species)
        self.gender = try! container.decode(String.self, forKey: .gender)
//        self.origin = try! container.decode(Dictionary.self, forKey: .origin)
//        self.location = try! container.decode(Dictionary.self, forKey: .location)
        self.image = try! container.decode(String.self, forKey: .image)
        self.episode = try! container.decode(List<String>.self, forKey: .episode)
        self.url = try! container.decode(String.self, forKey: .url)
        
        let originContainer = try container.nestedContainer(keyedBy: OriginLocationCodingKeys.self, forKey: .origin)
        self.origin = try! originContainer.decode(String.self, forKey: .name)
        
        let locationContainer = try container.nestedContainer(keyedBy: OriginLocationCodingKeys.self, forKey: .location)
        self.location = try! locationContainer.decode(String.self, forKey: .name)
    }
    
}

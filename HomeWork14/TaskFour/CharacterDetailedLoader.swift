//
//  CharacterDetailedLoader.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit
import Alamofire
import RealmSwift

class CharacterDetailedLoader:NSObject {
    
    func getCharacterFromRealm(id:Int) -> Character?{
        let realmInstence = try! Realm()
        let request = realmInstence.objects(Character.self).filter("id == \(id)")
        if request.count > 0 {
            return request.first
        } else {
            return nil
        }
    }
    
    func getCharacter(id:Int, completion: @escaping (Character) -> Void) {
        
        let url = "https://rickandmortyapi.com/api/character/\(id)"
        
        AF.request(url).validate().responseData {response in
            switch response.result {
            case .success(let value):
                if let character = try? JSONDecoder().decode(Character.self, from: value) {
                    DispatchQueue.main.async {
                        completion(character)
                    }
                }
                
                //print(self.charactersList)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getEpisodeName(url: String, completion: @escaping (String) -> Void) {
        AF.request(url).validate().responseJSON {response in
            switch response.result {
            case .success(let value):
                guard let data = value as? NSDictionary else { return }
                if let episodeName = data["name"] as? String {
                    DispatchQueue.main.async {
                        completion(episodeName)
                    }
                }
                
                //print(self.charactersList)
            case .failure(let error):
                print(error)
            }
        }
    }
}

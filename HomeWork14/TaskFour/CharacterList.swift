//
//  CharacterList.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit

class CharacterList: Decodable{
    
    var charactersCount: Int?
    var pages: Int?
    var nextPage: String?
    var prevPage: String?
    var characters: [Character]?
    
    enum CodingKeys: String, CodingKey {
        case info
        case characters = "results"
    }
    
    enum InfoCodingKeys: String, CodingKey {
        case charactersCount = "count"
        case pages
        case nextPage = "next"
        case prevPage = "prev"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.characters = try? container.decode([Character].self, forKey: .characters)
        let infoContainer = try container.nestedContainer(keyedBy: InfoCodingKeys.self, forKey: .info)
        self.charactersCount = try? infoContainer.decode(Int.self, forKey: .charactersCount)
        self.pages = try? infoContainer.decode(Int.self, forKey: .pages)
        self.nextPage = try? infoContainer.decode(String.self, forKey: .nextPage)
        self.prevPage = try? infoContainer.decode(String.self, forKey: .prevPage)
    }
}

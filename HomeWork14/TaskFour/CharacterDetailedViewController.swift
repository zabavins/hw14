//
//  CharacterDetailedViewController.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 04.10.2021.
//

import UIKit

class CharacterDetailedViewController: UIViewController {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var liveStatusLabel: UILabel!
    @IBOutlet weak var speciesAndGenderLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var firstSeenLabel: UILabel!
    @IBOutlet weak var liveStatusIndicatorView: UIView!
    
    var id = Int()
    var firstEpisodeUrl = ""
    let loader = CharacterDetailedLoader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        liveStatusIndicatorView.layer.cornerRadius = liveStatusIndicatorView.frame.width / 2
        
        if let character = loader.getCharacterFromRealm(id: id) {
            self.fillParams(character: character)
        } else {

            loader.getCharacter(id: id, completion: {character in
                self.fillParams(character: character)
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func fillParams(character: Character) {
        let url = URL(string: character.image)

        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url!){
                DispatchQueue.main.async {
                    self.avatarImageView.image = UIImage(data: data)
                }
                
            }
        }

        self.nameLabel.text = character.name
        self.liveStatusLabel.text = character.status
        if self.liveStatusLabel.text == "Alive" {
            self.liveStatusIndicatorView.backgroundColor = .green
        }
        self.speciesAndGenderLabel.text = "\(character.species) (\(character.gender))"

        self.locationLabel.text = character.location
        loader.getEpisodeName(url: character.episode[0], completion: {episodeName in
            self.firstSeenLabel.text = episodeName
        })
    }
}

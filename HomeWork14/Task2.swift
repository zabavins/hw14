//
//  Task2.swift
//  HomeWork14
//
//  Created by Сергей Забавин on 02.10.2021.
//

import UIKit
import CoreData

class Task3: NSManagedObject {
    
    var id = Int()
    var name = String()
    var desk = String()
    static func getTask(id: Int, name: String, descript: String) -> Task3{
        let task = Task3()
        task.id = id
        task.name = name
        task.desk = descript
        
        return task
    }

}
